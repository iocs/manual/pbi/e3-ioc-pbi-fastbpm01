require essioc
require wavepro, 0.1.5
require iocshutils

addScan .07  # ~14 Hz
addScan .05  # 20 Hz
addScan .03  # ~33 Hz

epicsEnvSet("FBPM_PREFIX", "PBI-FBPM01::")

epicsEnvSet("SCOPE_IP", "bd-scp02.tn.esss.lu.se")
epicsEnvSet("SCOPE_PREFIX", "PBI-FBCM01:PBI-Scope-001:")

# Load Wavepro Oscilloscope
iocshLoad("$(wavepro_DIR)/wavepro.iocsh", "PREFIX=$(SCOPE_PREFIX),DEVICE_IP=$(SCOPE_IP)")

# Load FastBPM records
dbLoadRecords("$(E3_CMD_TOP)/db/fastbpm.db", "P=$(FBPM_PREFIX),SCOPE=$(SCOPE_PREFIX)")

## For commands to be run after iocInit, use the function afterInit()

set_pass1_restoreFile("$(E3_CMD_TOP)/default_settings.sav", "P=$(FBPM_PREFIX),SCOPE=$(SCOPE_PREFIX)")

afterInit(dbpf "$(SCOPE_PREFIX)MemClear" "1")
afterInit(dbpf "$(SCOPE_PREFIX)Param1FuncUpdate" "1")

# Load general logging modules
iocshLoad("$(essioc_DIR)/common_config.iocsh")
